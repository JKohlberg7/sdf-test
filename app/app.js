var Express = require( "express" );
var app = Express();

app.get("/", (req, res) => {
    res.status(200).send( "Software Development Fundamentals." );
});

app.get("/test", (req, res) => {
    res.status(500).send("This is an error response.");
    console.log( 'The Server is Dead.' );
});

var server = app.listen(3000, () => {
    console.log( "Listening on port " + server.address().port + "..." );
});

module.exports = server;
