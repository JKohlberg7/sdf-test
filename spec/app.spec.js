var Request = require("request");

describe("Server", () => {
  var aux;

  beforeAll( () => {
    aux = require("../app/app");
  });

  afterAll( () => {
    aux.close();
  });

  it("should listen on port 3000", () => {
    expect(aux.address().port).toBe(3000);
  });

  
  it("", () => {
    const spy = axu.spyOn(app, 'get')
  });

  describe("GET /", () => {
    var data = {};

    beforeAll( (done) => {
      Request.get("http://localhost:3000/", (error, response, body) => {
        data.status = response.statusCode;
        data.body = body;
        done();
      });
    });

    it("Status 200", () => {
      expect(data.status).toBe(200);
    });

    it("should define body", () => {
      expect(data.status).toBeDefined();
    });

    it("Body", () => {
      expect(data.body).toBe("Software Development Fundamentals.");
    });

  });

  describe("GET /test", () => {
    var data = {};

    beforeAll( (done) => {
      Request.get("http://localhost:3000/test", (error, response, body) => {
        data.status = response.statusCode;
        data.body = body;
        done();
      });
    });

    it("Status 200", () => {
      expect(data.status).toBe(500);
    });

    it("should define body", () => {
      expect(data.status).toBeDefined();
    });

    it("Body", () => {
      expect(data.body).toBe("This is an error response.");
    });

  });
});
